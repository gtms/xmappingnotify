PREFIX = /usr/local
BINDIR = $(PREFIX)/bin

all: xmappingnotify

install: xmappingnotify
	@install -v -d "$(BINDIR)/" && install -m 0755 -v "./xmappingnotify" "$(BINDIR)/xmappingnotify"

uninstall:
	@rm -vrf "$(BINDIR)/xmappingnotify"

xmappingnotify: xmappingnotify.c
	gcc -Wall -O2 xmappingnotify.c -o xmappingnotify -lX11

clean: xmappingnotify
	rm xmappingnotify

.PHONY: all install uninstall clean
